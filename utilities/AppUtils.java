package com.utilities;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import testng.CaptureScreen;
import testng.LogAs;
import testng.Reports;
import testng.CaptureScreen.ScreenshotOf;

/**
 *This class contains application specific utilities
 */

public class AppUtils extends Utilities{
	
	
	/**
	*Function to validate the format of the Created field in the Manager Pages
	*/
	
	public void validateCreateFieldFormat(String pageName, String tblPdtFamily, String tblPdtFamilyBy){
		
		int colIndex = 0;
		String tblPdtFamilyColHeaders = tblPdtFamily + "/thead/tr/th";
		String tblPdtFamilyColHeadersBy = "xpath";
		String tblPdtFamilyRows = tblPdtFamily + "/tbody/tr";
		String tblPdtFamilyRowsBy = "xpath";

		List<WebElement> colHeaders = getListOfElements(tblPdtFamilyColHeaders,tblPdtFamilyColHeadersBy);
		
		for (int i = 0; i < colHeaders.size(); i++) {
			if(colHeaders.get(i).getText().trim().contains("CREATED")){
				colIndex = i+1;
				break;
			}
		}
		
		List<WebElement> rows = getListOfElements(tblPdtFamilyRows, tblPdtFamilyRowsBy);
		
		for (int j = 1; j <= rows.size(); j++) {
			
			WebElement createdCol = driver.findElement(By.xpath(tblPdtFamilyRows +"[" + j + "]/" + "td[" + colIndex + "]"));
			scrollIntoView(createdCol);
			String createdColData = createdCol.getText().trim();
//			Parker Lovelace (dc3112), 11/13/2017
			
			String[] firstSplitDataName = createdColData.split("\\(");
			String userName = firstSplitDataName[0].trim();
//			System.out.println(userName);
			
			String[] secondSplitDataUserName = firstSplitDataName[1].split("\\)");
			String attUID = secondSplitDataUserName[0].trim();
//			System.out.println(attUID);
			
			String[] thirdSplitDataDate = secondSplitDataUserName[1].split("\\,");
			String createdDate = thirdSplitDataDate[1].trim();
//			System.out.println(createdDate);
			
			if(validateOnlyLettersSpaces(userName) && validateAlphaNumeric(attUID) && validateDate(createdDate)){
				Reports.add("Pass","Created Field Format Validation"
						, "Created Field on the "+ pageName +" which has the data <strong>"+ createdColData +
						"</strong> follows the format Username (ATTUID), MM/DD/YYYY ",
						LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}else{
				Reports.add("Fail","Created Field Format Validation"
						, "Created Field on the "+ pageName +" which has the data <strong>"+ createdColData +
						"</strong> does not follow the format Username (ATTUID), MM/DD/YYYY ",
						LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				break;
			}
				
			
		}
		
	}
	
	
	/**
	*Function to add Notes in the Manager Pages
	*/
	public void addNotes(String addNoteText, String btnAddNotes, String locateByXpath, String txtNotes, String lblNotesWordCount, String btnSaveNotes)
	{
		try {
			getElement(btnAddNotes,locateByXpath).click();
			
//	   		Add Notes message 500 characters validation
	   		String twentyFiveLetterCombo = "abcdefghijklmnopqrstuvwxy";
	   		for(int i = 0; i <= 20; i++){
	   			getElement(txtNotes, locateByXpath).sendKeys(twentyFiveLetterCombo);
	   		}
	   		
			String hoverInputText = getElement(txtNotes, locateByXpath).getAttribute("value").trim();
			if(hoverInputText.length() == 500){
				Reports.add("Pass", "Add Notes Text Maximum 500 Characters Functionality",
						"Although user tries to add more than 500 characters, user is only able to enter 500 characters which is the expected functionality",
						LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				getElement(txtNotes, locateByXpath).clear();
			}else{
				Reports.add("Fail", "Add Notes Text Maximum 500 Characters Functionality",
						"When user tries to add more than 500 characters, user is able to enter more than 500 characters which is not the expected functionality",
						LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}	
			getElement(txtNotes, locateByXpath).clear();
			getElement(txtNotes,locateByXpath).sendKeys(addNoteText);
			Reports.add("Check","Notes remaining character count",getElement(lblNotesWordCount,locateByXpath).getText().toString(), LogAs.INFO, null);
		
			getElement(btnSaveNotes,locateByXpath).click();
			Reports.add("Pass","Add Notes","Successfully added Notes," + System.lineSeparator() + addNoteText, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		catch(Exception e) {
			Reports.add("Fail","Add Notes","Failed to Add Notes", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	
	/**
	*Function to Check entitlements and availability/unavailability of certain elements
	*@param locator - String Page Element locator
	*@param locatorType - String Page Element locator Type
	*@param entitlementType - String Type of entitlement
	*@param re	
	*/
	public void verifyEntitlements(String locator, String locatorType, String entitlementType, String reportDesc){

		try {
			if (entitlementType.equalsIgnoreCase("hide")) {
				if (waitForElement(locator, locatorType, 5)) {
					Reports.add("Fail","Verify that " + reportDesc + " is not visible to user",
							reportDesc + " is visible", LogAs.FAILED, 
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

				} else {
					Reports.add("Pass","Verify that " + reportDesc + " is not visible to user",
							reportDesc + " is not visible", LogAs.PASSED, 
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

			} else if (entitlementType.equalsIgnoreCase("readonly")){
				if (waitForElement(locator, locatorType, 10)) {
					Reports.add("Pass","Verify that " + reportDesc + " is visible to user",
							reportDesc + " is visible", LogAs.PASSED, 
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					Reports.add("Fail","Verify that " + reportDesc + " is visible to user",
							reportDesc + " module is not visible", LogAs.FAILED, 
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

			}else{
				Reports.add("Fail","Verify " + reportDesc + " entitlements",
						"Parameter does not match the criteria, please provide correct type of entitlements viz. HIDE, READ-ONLY and EDIT", LogAs.FAILED, 
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}

		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}


}
